# Count with Redux and Middleware

The demo illustrates a simple use case for Redux with middleware to handle asynchronous actions.

## Usage

Running locally requires installation of dependencies.

```bash
npm install
```

The project uses Vite and should be run using the run script:

```bash
npm run dev
```

## Author

Dewald Els