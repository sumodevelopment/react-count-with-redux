import {applyMiddleware, combineReducers, createStore} from "redux";
import {counterReducer} from "./features/Counter/counterState";
import {composeWithDevTools} from "redux-devtools-extension";
import {counterMiddleware} from "./features/Counter/counterState";

// Get all the reducers in the app
const rootReducers = combineReducers({
    counter: counterReducer
})

// Create the store and add dev tools with the middleware.
export default createStore(
    // Combined Reducers
    rootReducers, 
    // Middleware and Dev Tools
    composeWithDevTools(applyMiddleware(counterMiddleware)
))