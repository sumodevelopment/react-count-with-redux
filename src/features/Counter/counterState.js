// TYPES "Labelling" the Actions
export const ACTION_COUNTER_INCREMENT = '[counter] INCREMENT'
export const ACTION_COUNTER_DECREMENT = '[counter] DECREMENT'
export const ACTION_COUNTER_ATTEMPT_BOOST = '[counter] ATTEMPT_BOOST'
export const ACTION_COUNTER_BOOST = '[counter] BOOST'

/**----------------------------------------------------------------------
 * Actions
 -----------------------------------------------------------------------*/ 
// Increment
export const actionCounterIncrement = () => ({
    type: ACTION_COUNTER_INCREMENT
})
// Decrement
export const actionCounterDecrement = () => ({
    type: ACTION_COUNTER_DECREMENT
})

// Boost preparation - Will Start the Async Function
export const actionCounterAttemptBoost = (boostBy = 1) => ({
    type: ACTION_COUNTER_ATTEMPT_BOOST,
    payload: boostBy
})
// Boost - The Actual Boost
export const actionCounterBoost = (boostBy = 1) => ({
    type: ACTION_COUNTER_BOOST,
    payload: boostBy
})

/**----------------------------------------------------------------------
 * Reducer
 -----------------------------------------------------------------------*/ 
export const counterReducer = (state = 0, action) => {

    switch (action.type) {

        case ACTION_COUNTER_INCREMENT:
            return  state + 1
        case ACTION_COUNTER_DECREMENT:
            return  state - 1
        case ACTION_COUNTER_BOOST:
            return state + action.payload

        default:
            return state
    }

}

/**----------------------------------------------------------------------
 * Middleware
 -----------------------------------------------------------------------*/ 
export const counterMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_COUNTER_ATTEMPT_BOOST) {
        setTimeout(() => { 
            // Fake Async Code
            console.log('ACTION_COUNTER_BOOST --- Someone is boosting number! 🚀')
            // Dispatch the actual Boost action after 1 second.
            dispatch( actionCounterBoost(action.payload) )
            
        }, 1000)
    }

}